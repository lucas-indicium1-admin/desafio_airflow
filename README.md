# Resolução do Desafio Airflow

## O Problema

Construir uma DAG com as seguintes tasks:

- Extrai e carrega os dados de uma tabela para um arquivo `.csv`;
- Extrai os dados de outra tabela e funde com os dados do arquivo `.csv`;
- Realiza o cálculo de uma soma condicional e carrega o valor final em um arquivo.

## Pré-requisitos

Foram usadas apenas duas bibliotecas de terceiros:

- pandas
- numpy

Ambas constam no arquivo requirements.txt.

## Rodando a DAG no Airflow

Copie o arquivo `res_desafio.py` para a pasta `airflow-data/dags`. Acesse a página do Airflow e dê `Trigger Run`.