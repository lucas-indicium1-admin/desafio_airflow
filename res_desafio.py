from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable

# std import libraries
import sqlite3
import csv

# 3rd party import libraries
import pandas as pd
import numpy as np

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'lucas',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

## Do not change the code below this line ---------------------!!#
def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return None
## Do not change the code above this line-----------------------##

with DAG(
    'DesafioAirflow',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """

    # [START extract_sqlite_load_csv]
    def extract_sqlite_load_csv():
        con = sqlite3.connect("data/Northwind_small.sqlite")
        cur = con.cursor()

        export_csv = pd.read_sql_query(f"SELECT * FROM 'Order'", con)
        export_csv.to_csv("output_orders.csv",index=False,encoding="utf-8")

        con.close()
    # [END extract_sqlite_load_csv]

    # [START join_tables_export_count]
    def join_tables_export_count():
        con = sqlite3.connect("data/Northwind_small.sqlite")
        cur = con.cursor()

        read_orders_csv = pd.read_csv('output_orders.csv')

        extract_order_details = pd.read_sql_query(f"SELECT * FROM 'OrderDetail'", con)

        con.close()

        join_csv = pd.merge(
            extract_order_details,
            read_orders_csv,
            left_on=["OrderId"],
            right_on=["Id"],
            how='left',
            sort=False
            )

        final_count = join_csv[join_csv['ShipCity'] == 'Rio de Janeiro']['Quantity'].apply('sum').astype('str')

        with open('count.txt', 'w') as f:
            f.write(final_count)
    # [END join_tables_export_count]


    # [START main_flow]
    extract_sqlite_load_csv_task = PythonOperator(
        task_id='extract_sqlite_load_csv',
        python_callable=extract_sqlite_load_csv,
    )
    extract_sqlite_load_csv_task.doc_md = dedent(
        """\
    #### Extract and Load
    Essa task lê os dados da tabela 'Order' do banco de dados disponível
    em data/Northwhind_small.sqlite. O formato do banco de dados é o Sqlite3.
    No final, ela escreve um arquivo chamado "output_orders.csv" com o resultado da query.
    """
    )

    join_tables_export_count_task = PythonOperator(
        task_id='join_tables_export_count',
        python_callable=join_tables_export_count,
    )
    join_tables_export_count_task.doc_md = dedent(
        """\
    #### JOIN and Export count
    Essa task lê os dados da tabela "OrderDetail" do mesmo banco de dados e
    faz um JOIN com o arquivo "output_orders.csv" exportado na task anterior.
    Ela também calcula a soma da quantidade vendida (Quantity)
    com destino (ShipCity) para o Rio de Janeiro e exporta essa contagem em arquivo "count.txt".
    """
    )
    
    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

    extract_sqlite_load_csv_task >> join_tables_export_count_task >> export_final_output

# [END main_flow]